const Koa = require('koa');
const KoaRouter = require('koa-router');

const app = new Koa();
const router = new KoaRouter();

app.use(router.routes()).use(router.allowedMethods());

router.get('/logs', (ctx) => ctx.body = 'Hi from koa!');

app.listen(3000, () => console.log('Server has started'));